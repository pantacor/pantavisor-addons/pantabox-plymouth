#!/bin/sh

CONTAINER_IO_FS=/container-io
CONTAINER_QUIT_FILE=$CONTAINER_IO_FS/plymouth-quit
BOOT_MESSAGE="Booting Pantavisor.io"
TRY_PRINT_BOOT_MSG=
BOOT_MSG_PRINTED=
TRY_MSG_ONCE=

log()
{
	echo "[plymouth_script]: $*"
	echo "[plymouth_script]: $*" >> plymouth.log
}

active_wait()
{
	if [ -n "$TRY_PRINT_BOOT_MSG" ] && [ -z "$BOOT_MSG_PRINTED" ]; then
		# check if messages is buffered or an unbuffered message was sent
		if grep "on_display_message:displaying message ${BOOT_MESSAGE}" /tmp/plymouth_debug || grep "not displaying message ${BOOT_MESSAGE} as no splash" /tmp/plymouth_debug; then
			BOOT_MSG_PRINTED="y"
			log "...printing done!"
		else
			log "trying to print ..."
			plymouth display-message --text="${BOOT_MESSAGE}"
		fi
	fi

	sleep 1
}

prepare_plymouth()
{
	/sbin/mdev -s
	/sbin/udevd --daemon
	udevadm trigger

	/sbin/plymouthd --debug --no-daemon --mode=boot --kernel-command-line="plymouth.debug=stream:/tmp/plymouth_debug $(cat /proc/cmdline)" &
	pid=$!

	log "waiting for plymouth demon..."
	while ! plymouth --ping ; do
		active_wait
	done
	log "... daemon up!"
}

mkdir -p /dev
mount -t tmpfs none /dev

mkdir -p /sys
mount -t sysfs none /sys

prepare_plymouth

log "showing splash..."
plymouth show-splash --show-splash

TRY_PRINT_BOOT_MSG="y"
log "... done"

log "waiting for file $CONTAINER_QUIT_FILE ..."
while [ ! -f $CONTAINER_QUIT_FILE ]; do
	active_wait
done
log "... done"

log "exiting plymouth ..."
plymouth quit --retain-splash

# wait for plymouth to actually quit
while plymouth --ping ; do
	active_wait
done
rm $CONTAINER_QUIT_FILE

log "... done"

