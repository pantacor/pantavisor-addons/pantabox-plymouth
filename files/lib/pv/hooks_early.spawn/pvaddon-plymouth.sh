#!/bin/sh

PLYMOUTH_CHROOT_FS=plymouth
PLYMOUTH_CHROOT_IO_DIR=$PLYMOUTH_CHROOT_FS/container-io
PLYMOUTH_MOUNT=/volumes/_pv/addons/plymouth/text-io

mkdir -p plymouth_fs plymouth_ovl/upper plymouth_ovl/work plymouth_ovl/lower plymouth
mount -t squashfs /usr/local/lib/plymouth/root.squashfs /plymouth_fs
mount -t overlay -o lowerdir=/plymouth_fs,upperdir=/plymouth_ovl/upper,workdir=plymouth_ovl/work none /plymouth
mkdir -p $PLYMOUTH_MOUNT $PLYMOUTH_CHROOT_IO_DIR
mount --bind $PLYMOUTH_MOUNT $PLYMOUTH_CHROOT_IO_DIR
mount --bind /proc $PLYMOUTH_CHROOT_FS/proc
/sbin/chroot /$PLYMOUTH_CHROOT_FS /usr/local/bin/start_plymouth.sh
umount $PLYMOUTH_CHROOT_IO_DIR
umount /plymouth
umount /plymouth_fs
rm -rf plymouth_ovl

