FROM alpine as qemu

RUN if [ -n "arm" ]; then \
		wget -O /qemu-arm-static https://github.com/multiarch/qemu-user-static/releases/download/v4.1.0-1/qemu-arm-static; \
	else \
		echo '#!/bin/sh\n\ntrue' > /qemu-arm-static; \
	fi; \
	chmod a+x /qemu-arm-static

FROM arm32v7/alpine:3.12 as root

COPY --from=qemu /qemu-arm-static /usr/bin/

ADD files_fs/ /


RUN apk add plymouth plymouth-drm plymouth-themes elogind bash udev netcat-openbsd

RUN chmod +x /usr/local/bin/start_plymouth.sh

ADD themes /usr/share/plymouth/themes/

RUN /usr/sbin/plymouth-set-default-theme text

CMD [ "/usr/local/bin/start_plymouth.sh" ]


FROM debian as builder

# INSTALL BUILD DEPENDENCIES
RUN apt-get update && apt-get install -y \
	-qq --no-install-recommends \
	git \
	cpio \
	xz-utils \
	squashfs-tools

WORKDIR /work

COPY files/ /work/
COPY --from=root / /rootroot
# COPY --from=compiler /work/out/ /work/bin

# COPY SOURCE CODE
RUN find /work; mkdir -p /work/usr/local/lib/plymouth; mksquashfs /rootroot /work/usr/local/lib/plymouth/root.squashfs -comp xz

# 4-BYTE ALIGN CPIO.GZ
RUN mkdir /out; cd /work \
	&& find | cpio -H newc -ov | xz --keep --check=crc32 --lzma2=dict=512KiB  > /out/addon-plymouth.cpio.xz \
	&& file_size=`stat -c %s /out/addon-plymouth.cpio.xz` \
	&& remaining=$(($file_size%4)) \
	&& if [ $remaining = 0 ]; then new_size=$file_size; else new_size=$(($file_size+4-$remaining)); fi \
	&& dd if=/dev/zero of=/out/addon-plymouth.cpio.xz4 bs=1 count=$new_size \
	&& dd if=/out/addon-plymouth.cpio.xz of=/out/addon-plymouth.cpio.xz4 conv=notrunc


FROM alpine:3.10

# COPY BUILD ARTIFACTS
WORKDIR /out
COPY --from=builder /out/addon-plymouth.cpio.xz4 /out/addon-plymouth.cpio.xz4

ENTRYPOINT [ "/bin/tar", "-C", "/out", "-c", "." ]
